# Paperless Parts Coding Challenge: Fumihiro Tamada #

This is my solution to the paperless parts coding challege. I **learned** and used react in 2 days. It took approximately 5 hours for me to finish. I wrote a single-page application with React since I believe it is the best type of web-app for a dash-board view. Again, I never really used React and this is the first application writing an application of time. I know it is not perfect. But I hope you can see my learning ability.


## Project Objective ##

This project is a single application that displays the details of colleges in massachusetts. The colleges are displayed as a small card. To view the detail of the college, click the card and a modal will be popped out. Also, I depolyed **https://fumihiro-college-search.herokuapp.com/**.


### how to run ###

You will be working with three data files:

1. Clone the repo to your local repository
```git clone https://ftamada@bitbucket.org/ftamada/hiro-college-search.git``` 
2. Navigate to college-search repo
``` cd college-search```
3. Start the project using `npm`
``` npm start```

### Things that I could not do ###
* Writw unit tests 
* Make a button that can change the order of the cards
* Make a button that can change the way to display
