import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import CardBoard from './card-board';
import { Layout, Menu, Breadcrumb } from 'antd';
const { Header, Content, Footer } = Layout;


class App extends React.Component {

	constructor(props){
		super(props);
  		this.state = {
  			schools: require('./data/ma_schools.json'),
  			programs: require('./data/programs.json'),
  		};
  	}

  	render() {
    	return (
			<div>
			<Header>
			  <h1 className="Header">Hiro Massachusetts College Search</h1>
			</Header>
			<div>
				<CardBoard />
			</div>
			</div>
    	);
  	}
}

ReactDOM.render(<App />, document.getElementById('root'));