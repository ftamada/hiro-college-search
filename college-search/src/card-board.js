import React from 'react';
import ReactDOM from 'react-dom';
import { Card, Pagination,Input, Modal } from 'antd';
import 'antd/dist/antd.css';
import './card-board.css';
import { numEachPage, degreeMapper, CCSIZSETMapper, LOCALEMapper } from './constant';

class CardBoard extends React.Component {

	constructor(props){
		super(props);
		this.state = {
  			schools: require('./data/ma_schools.json'),
  			programs: require('./data/programs.json'),
  			filteredSchools: require('./data/ma_schools.json'),
  			searchingSchoolName: "",
  			minValue: 0,
      		maxValue: numEachPage,
      		modalVisible: false,  	
      		modalData: {}
      	};
  	}

  	pageChange = value => {
	    if (value <= 1) {
	      	this.setState({
	        minValue: 0,
	        maxValue: numEachPage
	    	});
	    } 
	    else {
	      	this.setState({
	        minValue: (value-1)*numEachPage,
	        maxValue: value * numEachPage
	      	});
	    }
	};

	collegeCompare = ( a, b ) => {
  		if ( a.INSTNM < b.INSTNM ){
    		return -1;
  		}
  		if ( a.INSTNM > b.INSTNM ){
    		return 1;
  		}
  		return 0;
	};

	filteringSchools = (e) => {
		this.setState({ searchingSchoolName: e.target.value });
		this.setState({
			filteredSchools: this.state.schools.filter(x => x.INSTNM.toLowerCase().includes(e.target.value.toLowerCase()))
		});
		console.log(e.target.value);
		//this.props.onChange(event.target.value);
	};

	showModal = (e) => {
		console.log(e);
    	this.setState({
      		modalVisible: true,
      		modalData: e
  		});
    };

  	handleOk = e => {
    	console.log(e);
    	this.setState({
  		modalVisible: false
    	});
  	};

  	handleCancel = e => {
    	console.log(e);
    	this.setState({
      	modalVisible: false
    	});
    	console.log(this.state.modalVisible);
  	};

  	render() {
  		const  schoolName  = this.state.searchingSchoolName;
  		const  targetSchool = this.state.schools[0];
    	return (
			<div style={{ background: '#ECECEC', padding: '30px' }}>
				<div style={{marginLeft: 10}}>
					<Input placeholder="Search here" id="schoolfiltering"
						value={schoolName}
						onChange = {this.filteringSchools}
						style={{ width: 200 }} 
						/>
				</div>
				<p>{this.searchSchool}</p>
				<div className = "college__cards__container">
					{this.state.filteredSchools && this.state.filteredSchools.length > 0 &&
						this.state.filteredSchools.sort(this.collegeCompare).slice(this.state.minValue, this.state.maxValue).map( value =>
						(
							<div className="college__card">
								<div onClick={() => this.showModal(value)}>
									<Card title={value.INSTNM} style={{ width: 300 }}>
					      				<p>City: {value.CITY}</p>
					      				<p>Degree: {degreeMapper[value.HIGHDEG]}</p>
					      				<p>Url: <a href={"//"+value.INSTURL}>{value.INSTURL}</a></p>
				   				 	</Card>
				   				</div>
			   				 	<Modal
						          title={this.state.modalData.INSTNM}
						          visible={this.state.modalVisible}
						          onOk={this.handleOk}
						          onCancel={this.handleCancel}
						          >
						         	<p>City: {this.state.modalData.CITY}</p>
					      			<p>Degree: {degreeMapper[this.state.modalData.HIGHDEG]}</p>
					      			<p>{LOCALEMapper[this.state.modalData.LOCALE]}</p>
					      			<p>Carnegie Classification: {CCSIZSETMapper[this.state.modalData.CCSIZSET]}</p>
					      			<p>Acceptance Rate: {(this.state.modalData.ADM_RATE!=="NULL")? (this.state.modalData.ADM_RATE*100).toFixed(0) + "%" : "N/A"}</p>
					      			<p>Programs:</p>
					      			<ul>{(this.state.modalData.PROGRAMS != undefined && this.state.modalData.PROGRAMS.length!=0)? 
					      				this.state.modalData.PROGRAMS.map(x => <li>{this.state.programs[x]}</li>):"N/A"}</ul>
					      			<p>Url: <a href={value.INSTURL}>{this.state.modalData.INSTURL}</a></p>
						        </Modal>
			   				</div>
		   				)
					)}
				</div>
				<Pagination
		          defaultCurrent={1}
		          defaultPageSize={numEachPage} //default size of page
		          onChange={this.pageChange}
		          total={this.state.filteredSchools.length} //total number of card data available
		        />
			</div>
    	);
  	}
}
export default CardBoard;
